# pull official base image
FROM python:3.8-slim

# set work directory
WORKDIR /usr/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH /usr/app

# copy requirements file
COPY ./pyproject.toml /usr/app/pyproject.toml

# install dependencies
RUN pip install poetry && \
    poetry config virtualenvs.create false && \
    poetry install --no-root --without dev && \
    rm -rf ~/.cache/pypoetry

# copy project
COPY . /usr/app
