import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _


class UUIDMixin(models.Model):
    id = models.UUIDField(_("ID"), default=uuid.uuid4, primary_key=True, editable=False)

    class Meta:
        abstract = True


class BaseModel(UUIDMixin):
    objects = models.Manager()

    class Meta:
        abstract = True
