from django.db import models

from src.helpers.models import BaseModel


class Brand(BaseModel):
    name = models.CharField(max_length=30, verbose_name="Brand's name")

    class Meta:
        db_table = "brands"
