# django-project



## Overview

This project is skeleton app for aiohttp site. Technical stack:

- **Django:** Web framework
- **Poetry:** Package manager
- **Pytest:** Run unit test with code coverage
- **Makefile:** Building project
- **Flake8, Mypy:** Lint code
- **Isort, Black, Autoflake:** Formating code
- **Docker:** Packaging and deployment
- **Helm:** Package manager for kubernetes
- **Gitlab CI:** CI/CD


## Quickstart

### Local run:
- first install poetry
```bash
pip install poetry
```
- install the dependencies locally
```bash
poetry install
```
- run server
```bash
make run
```
- run tests
```bash
make test
```
- db migrations
```bash
alembic upgrade head
```

### Run in docker
- run server
```bash
docker-compose up -d
```
- run tests
```bash
docker exec -it aiohttp_app pytest --verbose --cov=src --cov=tests
```
- db migrations
```bash
docker exec -it aiohttp_app alembic upgrade head
```
